# Get caller identity for account id attribute
data "aws_caller_identity" "core_shared_secrets" {
  provider = aws.core_shared_secret
}

# Create policy to allow ARNs to write to the secret
data "aws_iam_policy_document" "sm_policy_write" {
  statement {
    sid = "GrantsUpdatePermissionsToSecrets"
    actions = [
      "secretsmanager:GetSecretValue",
      "secretsmanager:DescribeSecret",
      "secretsmanager:PutSecretValue",
      "secretsmanager:ListSecrets",
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:GetResourcePolicy",
      "secretsmanager:UpdateSecretVersionStage",
      "secretsmanager:RotateSecret",
      "secretsmanager:CancelRotateSecret"
    ]
    resources = [aws_secretsmanager_secret.sm.arn]
    principals {
      identifiers = concat(var.write_arns, ["arn:aws:iam::${data.aws_caller_identity.core_shared_secrets.account_id}:role/OrgDeploy"]) # Ensure the account master role can access all secrets
      type        = "AWS"
    }
  }
}

# Create a policy for ARN specific Read-Only access
data "aws_iam_policy_document" "sm_policy_read_limited" {
  statement {
    sid = "GrantsReadPermissionsToSecrets"
    actions = [
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:GetSecretValue",
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetResourcePolicy",
    ]
    principals {
      identifiers = concat(var.read_arns, ["arn:aws:iam::${data.aws_caller_identity.core_shared_secrets.account_id}:role/OrgDeploy"]) # Ensure the account master role can access all secrets
      type        = "AWS"
    }
    resources = [aws_secretsmanager_secret.sm.arn]
  }
}

# Create a policy for ARN-wildcard specific Read-Only access (for terraform workspaces)
data "aws_iam_policy_document" "sm_policy_read_wildcards" {
  statement {
    sid = "ReadPermissionsForWildcardPrincipals"
    actions = [
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:GetSecretValue",
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetResourcePolicy",
    ]
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    resources = [aws_secretsmanager_secret.sm.arn]
    condition {
      test     = "ArnLike"
      variable = "aws:PrincipalArn"
      values   = var.read_arn_wildcards
    }
  }
}

# Create a policy for Org-Wide Read-Only access
data "aws_iam_policy_document" "sm_policy_read_org" {
  statement {
    sid = "GrantsReadPermissionsToSecrets"
    actions = [
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:GetSecretValue",
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetResourcePolicy",
    ]
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    resources = [aws_secretsmanager_secret.sm.arn]
    condition { # Restrict to only our AWS Organization
      test     = "StringEquals"
      variable = "aws:PrincipalOrgID"
      values   = [var.share_to_organization]
    }
  }
}

# Merge policy document to create the final policy
data "aws_iam_policy_document" "sm_policy" {
  source_policy_documents = compact([
    data.aws_iam_policy_document.sm_policy_write.json,
    var.share_to_organization != "" ? data.aws_iam_policy_document.sm_policy_read_org.json : data.aws_iam_policy_document.sm_policy_read_limited.json,
    length(var.read_arn_wildcards) > 0 ? data.aws_iam_policy_document.sm_policy_read_wildcards.json : null,
  ])
}

# Attach the final policy document to the secret
resource "aws_secretsmanager_secret_policy" "sm_policy" {
  provider = aws.core_shared_secret

  secret_arn = aws_secretsmanager_secret.sm.arn
  policy     = data.aws_iam_policy_document.sm_policy.json
}
