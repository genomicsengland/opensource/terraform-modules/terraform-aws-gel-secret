# Terraform-AWS-GEL-Secret

Create secrets in our Core_Shared account

GEL policy requires all secrets to be created in our Core_Shared account, and be
encrypted with a dedicated KMS key.  Secrets should also have limited access
since they may contain sensitive information (hence being a secret)

This module abstracts the complexity of creating the required Secret Policy
which restricts access.  It also ensures secrets are created using a
cross-account role which is dedicated to creating secrets in our organisation.

Secrets can be created with either dedicated Read ARNs, or using an Org ID to
make it readable from anywhere in the Organization.  Occasionally there are
secrets that need to be used in so many projects, creating them with specific
ARNs becomes problematic.  This option should be avoided if possible.  

> *Note:* If the `share_to_organization` parameter is used, `read_arns` will be
ignored.

# Examples

## Application Secret

```
module "app_secret" {
  source    = "<path to module>"
  providers = {
    aws.core_shared_secret = aws.core_shared_secret
  }

  description = "Secret value stored in secrets manager in core shared account"
  kms_key_id  = "<ARN of the secrets manager Key>"

  account_env  = "prod"
  account_name = "example_prod"
  application  = "application"
  secret_name  = "mySecret"

  write_arns = []
  read_arns  = [aws_iam_role.example_role.arn]
}
```

creates a secret named `/prod/example_prod/application/mySecret`

## Secret with wildcard access (for terraform workspaces)

This supports the use of terraform workspaces. The reason for this is as follows:

- When you create a Secrets Manager secret via terraform, you (at least within GEL) have to populate it yourself manually
- It therefore doesn't make sense to create the resource every time in a workspace if you're using terraform workspaces
- Instead, you create the resource in the canonical environment, and use a data source to access it when you're running terraform in a workspace
- This works fine, except that when you create your IAM roles in your workspace they'll be called something like `the-iam-role-workspace` rather than just `the-iam-role`
- So - in order to allow your IAM roles in terraform workspaces to access your canonical Secrets Manager secrets, you need to add wildcard principal ARNs to your Secrets Manager resource policies - e.g. `the-iam-role*`

```
module "app_secret" {
  source    = "<path to module>"
  providers = {
    aws.core_shared_secret = aws.core_shared_secret
  }

  description = "Secret with wildcard permissions"
  kms_key_id  = "<ARN of the secrets manager Key>"
  account_env  = "prod"
  account_name = "example_prod"
  application  = "application"
  secret_name  = "applicationSecret"

  write_arns = []
  read_arns          = ["arn:aws:iam::${var.account_id}:role/CIDeploy"]
  read_arn_wildcards = ["arn:aws:iam::${var.account_id}:role/application-iam-role-*"]
}
```

## Organization-Wide Secret

```
module "app_secret" {
  source    = "<path to module>"
  providers = {
    aws.core_shared_secret = aws.core_shared_secret
  }

  description = "Org-Wide Secret"
  kms_key_id  = "<ARN of the secrets manager Key>"

  account_env  = "prod"
  account_name = "example_prod"
  application  = "application"
  secret_name  = "orgSecret"

  write_arns = []
  share_to_organization  = "o-01234abcde"
}
```

creates a secret named `/prod/example_prod/application/orgSecret`

## Maintain Secret Versions using Secrets Module

```
resource "random_password" "password" {
  count            = 6
  length           = 25
  special          = true
  override_special = "()*+,-.;<=>^_"
}

resource "aws_secretsmanager_secret_version" "example_secret" {
  depends_on = [
    module.example_secret,
  ]
  secret_id     = module.example_secret.arn
  secret_string = random_password.password[0].result
}
```

# Argument Reference

| Attribute | Description | Required | Default Value |
|---|---|:---:|:---:|
| `account_env` | Environment Organizational Unit, for example: dev, test etc | Yes |  |
| `account_name` | Target Account Name | Yes |  |
| `application` | Application Name | Yes |  |
| `secret_name` | Secret Unique Identifier | Yes |  |
| `description` | Description of the Secret | No | `""` |
| `kms_key_id` | KMS Key to encrypt the secret | Yes | GEL Secrets KMS CMK |
| `read_arns` | ARNs allowed to read this Secret | No | `[]` |
| `write_arns` | ARNs allowed to Write to this Secret | No | `[]` |
| `share_to_organization` | Share Secret to whole Organization (overrides write_arns) | No | `"` |
| `recovery_window_in_days` | Recovery window (in days) after Secret deletion | No | `7` |

# Attribute Reference

| Attribute | Description |
|---|---|
| `id` | Secret Id |
| `arn` | Secret ARN |
| `name` | Secret Name |

# Confluence Docs
- [Creating Secrets with GEL Module](https://cnfl.extge.co.uk/display/GOLD/Creating+Secrets+with+GEL+Module)
