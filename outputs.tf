output "arn" {
  value = aws_secretsmanager_secret.sm.arn
}

output "id" {
  value = aws_secretsmanager_secret.sm.id
}

output "name" {
  value = aws_secretsmanager_secret.sm.name
}