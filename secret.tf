locals {
  secret_name = "/${var.account_env}/${var.account_name}/${var.application}/${var.secret_name}"
}

resource "aws_secretsmanager_secret" "sm" {
  provider = aws.core_shared_secret

  name                    = local.secret_name
  description             = var.description
  kms_key_id              = var.kms_key_id
  recovery_window_in_days = var.share_to_organization != "" ? 30 : var.recovery_window_in_days

  tags = {
    Name                = local.secret_name
    Application         = var.application
    AWS_OU_environment  = var.account_env
    Secret_account_name = var.account_name
  }
}
