module "example_secret" {
  source = "../"
  providers = {
    aws.core_shared_secret = aws.core_shared_secret
  }

  kms_key_id   = "kms_key_id"
  account_env  = "environment"
  account_name = "account_name"
  application  = "application"
  secret_name  = "example_secret"

  recovery_window_in_days = 7

  description = "Example Secret for Terraform-AWS-GEL-Secret Module"

  write_arns            = [] # By default only core_shared admins will be able to write to this secret
  share_to_organization = var.organization_id
}

module "default_tags" {
  source = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/gel-standard-tagging.git?ref=latest"

  tribe       = "my tribe"
  gel_service = "my gel service"
  service     = "my service"
  squad       = "my squad"
  environment = var.environment
}

resource "random_password" "password" {
  count            = 6
  length           = 25
  special          = true
  override_special = "()*+,-.;<=>^_"
}

resource "aws_secretsmanager_secret_version" "example_secret" {
  depends_on = [
    module.example_secret,
  ]
  secret_id     = module.example_secret.arn
  secret_string = random_password.password[0].result
}
