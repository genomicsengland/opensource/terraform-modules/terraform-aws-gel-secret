terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.63.0"
    }
  }
  required_version = ">= 1.0.0"
}

provider "aws" {
  region = "eu-west-2"
}

provider "aws" {
  alias  = "core_shared_secret"
  region = "eu-west-2"
  assume_role {
    role_arn = "arn:aws:iam::${var.core_shared_id}:role/CIDeployGELSecret"
  }
  default_tags {
    tags = module.default_tags.tags
  }
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}
