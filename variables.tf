variable "account_env" {
  type        = string
  description = "Environment Organizational Unit, for example: dev, test etc."
}

variable "account_name" {
  type        = string
  description = "Target Account Name"
}

variable "application" {
  type        = string
  description = "Application Name"
}

variable "secret_name" {
  type        = string
  description = "Secret Unique Identifier"
}

variable "description" {
  type        = string
  description = "Description of the Secret"
  default     = ""
}

variable "read_arns" {
  type        = list(string)
  description = "ARNs allowed to read this Secret"
  default     = []
}

variable "read_arn_wildcards" {
  type        = list(string)
  description = "Wildcards for ARNs allowed to read this Secret. Needed for terraform workspaces."
  default     = []
}

variable "kms_key_id" {
  type        = string
  description = "KMS Key to encrypt the secret"
}

variable "write_arns" {
  type        = list(string)
  description = "ARNs allowed to Write to this Secret"
  default     = []
}

variable "share_to_organization" {
  type        = string
  description = "Share Secret to whole Organization (overrides write_arns)"
  default     = ""
}

variable "recovery_window_in_days" {
  type        = number
  description = "Recovery window (in days) after Secret deletion"
  default     = 7
}
